"""
=========================
BAG2 Mos parameter module 
=========================

Collection of MOS transistor related technology parameters for 
BAG2 framework

The goal of the class is to provide a master source for the MOS parameters 
and enable documentation of those parameters with docstrings.

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 08.04.2022.

"""
import os
import pkg_resources
import yaml

#To use this file as starting point for mos_parameters class comment the following lines
from abc import ABCMeta, abstractmethod
class mos_parameters_template(metaclass=ABCMeta):

# Then  uncomment the followint lines
#from BAG2_technology_template.mos_parameters_template import mos_parameters_template
#class mos_parameters(mos_parameters_template):

# Then, delete the lines with '@abstractmethod' and start filling in the right values

    @property
    @abstractmethod
    def mos_pitch(self):
        """OD quantization pitch.

        """
        return int 

    @property
    @abstractmethod
    def analog_unit_fg(self):
        """Number of fingers in an AnalogBase row must be multiples of this number.

        """
        return int 

    @property
    @abstractmethod
    def draw_zero_extension(self): 
        """True if zero height AnalogMosExt should be drawn.
        
        """
        return bool

    @property
    @abstractmethod
    def floating_dummy(self):
        """True if floating dummies are allowed.
        
        """
        return bool

    @property
    @abstractmethod
    def abut_analog_mos(self):
        """True if AnalogMosConn can abut each other.
        
        """
        return bool

    @property
    @abstractmethod
    def draw_sub_od(self):
        """True to draw OD in substrate contact.  False generally used to draw SOI transistors.
        
        """
        return bool
    
    @property
    @abstractmethod
    def sub_ring_lch(self):
        """Channel length used in substrate rings.
        
        """
        return float 
    
    @property
    @abstractmethod
    def dum_conn_pitch (self):
        """Vertical dummy wires pitch, in routing grid tracks.
        
        """
        return int 
    
    @property
    @abstractmethod
    def dum_layer(self):
        """Dummy connection layer.
        
        """
        return int 
    
    @property
    @abstractmethod
    def ana_conn_layer(self):
        """AnalogBase vertical connection layer.
        
        """
        return int 

    @property
    @abstractmethod
    def dig_conn_layer(self):
        """LaygoBase vertical connection layer
        
        """
        return int 
    
    @property
    @abstractmethod
    def dig_top_layer(self):
        """LaygoBase top layer.
        
        """
        return int 
    
    @property
    @abstractmethod
    def imp_od_encx(self):
        """Horizontal enclosure of implant layers over OD
        
        """
        return int 
    
    @property
    @abstractmethod
    def od_spy_max(self):
        """Maximum space between OD rows.
        
        """
        return int 

    @property
    @abstractmethod
    def od_min_density(self):
        """Minimum OD density
        
        """
        return float 
    
    @property
    @abstractmethod
    def od_fill_w(self):
        """Dummy OD height range.
        
        """
        return (int , int )

    @property
    @abstractmethod
    def md_w(self):
        """Width of bottom bound-box of OD-METAL1 via
        Defines also x width of the quard ring
        
        """
        return int 

    @property
    @abstractmethod
    def od_spy(self):
        """Controls the distance between substrate contact and 

        """
        #return int 
        return int 

    @property
    @abstractmethod
    def imp_od_ency(self):
        """Implant layers vertical enclosure of active.
        this is used to figure out where to separate implant layers in extension blocks,
        """
        return int 

    @property
    @abstractmethod
    def imp_po_ency(self):
        """Implant layers vertical enclosure of poly.
        this is used to figure out where to separate implant layers in extension blocks,
        if None, this rule is ignored.
        Does not seem to affect anything
        
        """
        return int 

    @property
    @abstractmethod
    def nw_dnw_ovl(self):
        """Overlap between N-well layer and Deep N-well layer.
        
        """
        return int 

    @property
    @abstractmethod
    def nw_dnw_ext(self):
        """Extension of N-well layer over Deep N-well layer.
        
        """
        return int 

    @property
    @abstractmethod
    def min_fg_decap(self):
        """Minimum number of fingers for decap connection.
        
        """
        return { 'lch' : [float('inf')],
                 'val' : [int ]
               }

    @property
    @abstractmethod
    def min_fg_sep(self):
        """Minimum number of fingers between separate AnalogMosConn.
        
        """
        return { 'lch': [float('inf')],
                 'val': [int ]
               }

    @property
    @abstractmethod
    def edge_margin(self):
        """Space between AnalogBase implant and boundary.
        
        """
        return { 'lch' : [float('inf')],
               'val' : [int ]
             }

    @property
    @abstractmethod
    def fg_gr_min(self):
        """Minimum number of fingers needed for left/right guard ring.
        
        """
        return { 'lch': [int , float('inf')],
                 'val' :  [int , int ]
        }

    @property
    @abstractmethod
    def fg_outer_min(self):
        """Minimum number of fingers in outer edge block.
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
       }

    @property
    @abstractmethod
    def sd_pitch_constants(self):
        """Source/drain pitch related constants.
        source/drain pitch is computed as val[0] + val[1] * lch_unit

        """
        return { 'lch': [int , float('inf')],
                 'val' :  [[int , int ],[int , int ]]
       }

    @property
    @abstractmethod
    def num_sd_per_track(self):
        """Number of source/drain junction per vertical track.
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
       }

    @property
    @abstractmethod
    def po_spy(self):
        """Space between PO
        
        """
        return { 'lch': [int , float('inf')],
                 'val' :  [int , int ]
       }

    @property
    @abstractmethod
    def mx_gd_spy(self):
        """Vertical space between gate/drain metal wires.
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
       }

    @property
    @abstractmethod
    def od_gd_spy(self):
        """Space between gate wire and OD
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
       }

    @property
    @abstractmethod
    def po_od_exty(self):
        """PO extension over OD
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }


    @property
    @abstractmethod
    def po_od_extx_constants(self):
        """OD horizontal extension over PO.
        value specified as a (offset, lch_scale, sd_pitch_scale) tuple, 
        where the extension is computed as 
        offset + lch_scale * lch_unit + sd_pitch_scale * sd_pitch_unit
        
        DEPRECATED: specify a constant number directly.
        (What does this mean)

        """
        return { 'lch': [int, float('inf')],
                 'val':  [(int, int, int), (int, int, int)]
        }


    @property
    @abstractmethod
    def po_h_min(self):
        """Minimum PO height

        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }

    @property
    @abstractmethod
    def sub_m1_extx(self):
        """Distance between substrate METAL1 left edge to
        center of left-most source-drain junction.
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }


    @property
    @abstractmethod
    def sub_m1_enc_le(self):
        """Substrate METAL1 via line-end enclsoure
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }

    @property
    @abstractmethod
    def dum_m1_encx(self):
        """Dummy METAL1 horizontal enclosure.
        
        """
        return { 'lch': [float('inf')],
                 'val' :  [int ]
        }


    @property
    @abstractmethod
    def g_bot_layer(self):
        """Gate wire bottom layer ID
        
        """
        return int 

    @property
    @abstractmethod
    def d_bot_layer(self):
        """Drain/source wire bottom layer ID
        
        """
        return int 

    @property
    @abstractmethod
    def g_conn_w(self):
        """Gate vertical wire width on each layer.
        
        """
        return { 'lch': [float('inf')],
                 'val' : [[int , int , int ],
                     ]
               }

    @property
    @abstractmethod
    def d_conn_w(self):
        """Drain vertical wire width on each layer.
        
        """
        return { 'lch': [float('inf')],
                 'val': [[int , int , int ]
                     ]
               }

    @property
    @abstractmethod
    def g_conn_dir(self):
        """Gate wire directions
        
        """
        return ['x', 'y', 'y']


    @property
    @abstractmethod
    def d_conn_dir(self):
        """Drain wire directions
        
        """
        return ['y', 'y', 'y']

    @property
    @abstractmethod
    def g_via(self):
        """Gate via parameters
        
        """
        return { 'dim' : [ 
            [int , int ],
            [int , int ],
            [int , int ]
            ],
        'sp' : [int , int , int ],
        #This is provided not to break old generators
        'bot_enc_le' : [int , int , int ],
        #This can be used to control gate poly extension
        'bot_enc_le_top' : [int , int , int ],
        'bot_enc_le_bot' : [int , int , int ],
        'top_enc_le' : [int , int , int ]
       }

    @property
    @abstractmethod
    def prim_offset(self):
        """Y direction offset for the primitive used in Analog base

        Currently used at Aalto
           
        """
        return int

    @property
    @abstractmethod
    def d_via(self):
        """Drain/source via parameters
  
        """
    #Minimum dimension of vias from PO to mos_conn_layer -1 (2)
        return { 'dim': [ 
            [int , int ],
            [int , int ], 
            [int , int ]
        ],
        #Minimum spacing of vias
        'sp' : [int , int , int ],
        'bot_enc_le' : [int , int , int ],
        'top_enc_le' : [int , int , int ]
    }

    @property
    @abstractmethod
    def substrate_contact(self):
        """Substrate contact parameter 
        Copy d_via here if needed. Used Analog Base if 
        substrate contacs differ from drain contacts

        """
        return { 'dim': [ 
            [int, int ],
            [int, int ], 
            [int, int ]
        ],
        #Minimum spacing of vias
        'sp' : [int, int ],
        'bot_enc_le' : [int, int],
        'top_enc_le' : [int, int]
    }

    @property
    @abstractmethod
    def dnw_layers(self):
        """Deep N-well layer names
        
        """
        return [ ('layer', 'purpose'),
                ]
    @property
    @abstractmethod
    def imp_layers(self):
        """Implant layer names for each transistor/substrate tap type.
        
        """
        return { 'nch_thick' : {
              ('layer', 'purpose') : [0,0],
              ('layer', 'purpose') : [0,0]
          },
          'nch' : {
              ('layer', 'purpose') : [0, 0]
          },
          'pch': {
               ('layer', 'purpose') : [0, 0],
               ('layer', 'purpose'): [0, 0],
               ('layer', 'purpose'): [0, 0]
          },
          'ptap' : {
            ('layer', 'purpose'): [0, 0]
          },
          'ntap' : {
            ('layer', 'purpose') : [0, 0],
            ('layer', 'purpose'): [0, 0],
            ('layer', 'purpose'): [0, 0]
          }
    }

    @property
    @abstractmethod
    def thres_layers(self):
        """Threshold layer names for each transistor/substrate tap type.
        
        """
        return { 
            'nch' : {
              'standard' : {},
              'svt' : {},
              'lvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'ulvt' : { 
                  ('layer', 'purpose') : [0, 0]
                  },
              'fast' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'hvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'uhvt' : {
                  ('layer', 'purpose') : [0, 0]
                  }
             },
            'pch' : {
              'standard' : {},
              'svt' : {},
              'lvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'ulvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'fast' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'hvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'uhvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
             },
            'ptap' : {
              'standard' : {},
              'svt' : {},
              'lvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'ulvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'fast' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'hvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'uhvt' : {
                  ('layer', 'purpose') : [0, 0]
                  }
            },
            'ntap' : {
              'standard' : {},
              'svt' : {},
              'lvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'ulvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'fast' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'hvt' : {
                  ('layer', 'purpose') : [0, 0]
                  },
              'uhvt' : {
                  ('layer', 'purpose') : [0, 0]
                  }
            }
        }
    
    @property
    @abstractmethod
    def fin_h(self):
        """FINFET specific parameter fin height

        """
        return int

    @property
    @abstractmethod
    def od_spx(self):
        """FINFET specific parameter minimum horizontal space between OD, in resolution units

        """
        return int

    @property
    @abstractmethod
    def od_fin_exty_constants(self):
        """Optional: OD vertical extension over fins FINFET specific parameter

        """
        return [int, int, int]


    @property
    @abstractmethod
    def  od_fin_extx(self):
        """Horizontal enclosure of fins over OD FINFET specific parameter

        """
        return int

    @property
    @abstractmethod
    def no_sub_dummy(self):
        """True if dummies cannot be drawn on substrate region FINFET specific parameter

        """
        return bool

    @property
    @abstractmethod
    def  mos_conn_modulus(self):
        """Source/drain modulus for transistor/dummy connections.


        Allows transistor/dummy connections to change their geometry based
        on the source column index modulo by this number.

        Default 1
        """
        return 1

    @property
    @abstractmethod
    def  od_fill_h(self):
        """Dummy OD height range FINFET specific parameter

        """
        return [int, int]

    @property
    @abstractmethod
    def od_fill_w_max(self):
        """Dummy OD maximum width, in resolution units FINFET specific parameter

        """
        return None 


    @property
    @abstractmethod
    def imp_min_w(self):
        """Minimum implant layer width FINFET specific parameter

        """
        return int

    @property
    @abstractmethod
    def  imp_edge_dx(self):
        """Dictionary from implant layers to X-delta in outer edge blocks FINFET specific parameter

        """
        return  {('layer', 'property') : [int, int ]}

    @property
    @abstractmethod
    def mp_h_sub(self):
        """Substrate MP height FINFET specific parameter

        """
        return int 

    @property
    @abstractmethod
    def mp_spy_sub(self):
        """Substrate MP vertical space FINFET specific parameter

        """
        return int
    
    @property
    @abstractmethod
    def mp_po_ovl_constants_sub(self):
        """Substrate MP extension/overlap over PO FINFET specific parameter

        """
        return [int, int] 

    @property
    @abstractmethod
    def mp_md_sp_sub(self):
        """Substrate MP space to MD FINFET specific parameter

        """
        return int
    
    @property
    @abstractmethod
    def mp_cpo_sp_sub(self):
        """Substrate MP space to CPO FINFET specific parameter

        """
        return int

    @property
    @abstractmethod
    def mp_h(self):
        """MP height FINFET specific parameter

        """
        return int   
    
    @property
    @abstractmethod
    def mp_spy(self):
        """Vertical space between MP FINFET specific parameter

        """
        return int

    @property
    @abstractmethod
    def mp_po_ovl_constants(self):
        """MP and PO overlap FINFET specific parameter

        """
        return [int, int]    

    @property
    @abstractmethod
    def mp_md_sp(self):
        """Space bewteen MP and MD FINFET specific parameter

        """
        return int

    @property
    @abstractmethod
    def mp_cpo_sp(self):
        """Space between MP and CPO FINFET specific parameter

        """
        return int   

    @property
    @abstractmethod
    def  has_cpo(self):
        """True to draw CPO FINFET specific parameter

        """
        return bool
 
    @property
    @abstractmethod
    def cpo_h(self):
        """Normal CPO height FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def cpo_po_extx(self):
        """Horizontal extension of CPO beyond PO FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def cpo_po_ency(self):
        """Vertical enclosure of CPO on PO FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def cpo_od_sp(self):
        """CPO to OD spacing FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def cpo_spy(self):
        """CPO to CPO vertical spacing FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def cpo_h_end(self):
        """CPO height for substrate end FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def  md_od_exty(self):
        """Vertical extension of MD over OD FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def md_spy(self):
        """Vertical space bewteen MD FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def md_h_min(self):
        """Minimum height of MD FINFET specific parameter

        """
        return { 'lch' : [float('inf')],
                 'val' : [int]
               }

    @property
    @abstractmethod
    def  dpo_edge_spy(self):
        """Vertical space between gate PO when no CPO is used for dummy transistors FINFET specific parameter

        """
        return int
 
    @property
    @abstractmethod
    def  g_m1_dum_h(self):
        """Gate M1 dummy wire height FINFET specific parameter

        """
        return int

    @property
    @abstractmethod
    def  ds_m2_sp(self):
        """Drain/source M2 space FINFET specific parameter

        """
        return int



    @property
    @abstractmethod
    def property_dict(self):
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        devel = False 
        #These are for development and debugging
        if not hasattr(self, '_property_dict'):
            if devel:
                yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
                with open(yaml_file, 'r') as content:
                    dictionary = yaml.load(content, Loader=yaml.FullLoader )
                    self._property_dict = dictionary['mos']
            else:
                self._property_dict = {}

        for key, val  in vars(type(self)).items():
            if isinstance(val,property) and key != 'property_dict':
                self._property_dict[key] = getattr(self,key)
        return self._property_dict

