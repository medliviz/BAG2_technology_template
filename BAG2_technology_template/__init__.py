"""
===================================
BAG2 technology definition template
===================================
Class to define the technology for BAG2 framework

To use this template for technology definition, add this class as a submodule under name
BAG2_technology_definition, and follow the instruction given in comments

Initially written by Marko Kosunen, Aalto University, 2019.

"""

import os

import pkg_resources
import yaml
import importlib
import copy

from bag.layout.tech import TechInfoConfig

from .analog_mos.planar import MOSTechPlanarGeneric
from .resistor.planar import ResTechPlanarGeneric
# To use this as a BAG2_technology_definition class, comment the following lines
from .layer_parameters_template import layer_parameters_template as layer_parameters
from .mos_parameters_template import mos_parameters_template as mos_parameters
from .via_parameters_template import via_parameters_template as via_parameters
from .resistor_parameters_template import resistor_parameters_template as resistor_parameters

#    Then uncomment the following lines
#from .layer_parameters import layer_parameters
#from .mos_parameters import mos_parameters
#from .via_parameters import via_parameters
#from .resistor_parameters import resistor_parameters

#    Then comment the following line
class BAG2_technology_template(TechInfoConfig):
#    Then un comment the following line
#class BAG2_technology_definition(TechInfoConfig):

    def __init__(self, process_params):
        TechInfoConfig.__init__(self, self.process_config, process_params)
        # These are the original settings
        process_params['layout']['mos_tech_class'] = MOSTechPlanarGeneric(self.process_config, self)
        process_params['layout']['res_tech_class'] = ResTechPlanarGeneric(self.process_config, self)

    @property
    def process_config(self):
        """ This is an property that parses through all the process parameters, 
        assigns them to Dictionary '_config', and returns that dict.

        """
        # devel variable can be used in process of porting old tech_params.yaml files to class properties.
        devel = False
        if not hasattr(self, '_config') or not hasattr(self,'_process_config'):
            if devel:
                _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))

                with open(_yaml_file, 'r') as content:
                    self._process_config = yaml.load(content, Loader=yaml.FullLoader)
            else:
                ## Initialize the comnpatibility Dict, originally read from Yaml file
                self._process_config = {}

            layers=layer_parameters()
            ## Update the dictionary by copying the layers dict to it
            self._process_config.update(layers.property_dict)
            mos=mos_parameters()
            via=via_parameters()
            resistor=resistor_parameters()
            self._process_config['mos']= mos.property_dict
            self._process_config['via']= via.property_dict
            # Via parameters should be isolated inside via class.
            # These are for compatibility 
            self._process_config['via_name'] = via.property_dict['via_name']
            self._process_config['via_id'] = via.property_dict['via_id']
            self._process_config['resistor']= resistor.property_dict

        self._config=self._process_config
        return self._process_config

    @property
    def grid_opts(self):
        """Dict : Global routing track definition options for the process.
        Utilized by bag_ecd.bag_desing.routing_grid.
        'num' is a placeholder for a numeric value.
        
        Keys: 
                bot_dir : y|x , Direction fo the bottom routing layer
                layers  : list of available layer numbers
                spaces  : default spacing of the routing layers
                widths  : default widths of routing layers
                with_override: Dict of Dicts. Purpose unclear.

        """
        if not hasattr(self,'_grid_opts'):
            self._grid_opts={'bot_dir': 'y',
              'layers': [1, 2, 3, 4, 5, 6, 7, 8, 9,10,11],
              'spaces': [num, num, num, num, num, num, num, num, num,num ,num ],
              'widths': [num, num, num, num, num, num, num, num, num, num,num ],
              'width_override': {2: {2: num},
               3: {2: num},
               4: {2: num},
               5: {2: num},
               6: {2: num},
               7: {2: num},
               8: {3: num}},
              }
            return self._grid_opts
        else:
            return self._grid_opts
    @property
    def min_lch(self):
        """ Minimum lch for transistors. Generate your designs using multiples of this
        
        """ 
        if not hasattr(self,'_min_lch'):
            self._min_lch= num
            return self._min_lch
        else:
            return self._min_lch

    @property
    def primitives(self):
        if not hasattr(self, '_generators'):
            self._generators = ['nameofthedevicegen', 'nameofthedevicegen']
        return self._generators
    
    @property
    def min_w(self):
        if not hasattr(self, '_min_w'):
            libs=[importlib.import_module('BAG_prim.layouts.%s' % gen) for gen in self.primitives]
            self._min_w={}
            for lib, gen in zip(libs, self.primitives):
                cls = getattr(lib, gen)
                self._min_w.update({gen : cls.min_w()})
        return self._min_w

    
    # Methods
    def get_transistor_primitives(self,**kwargs):
        """
        Maps given flavor and type to a name of a vendor primitive
        this can be used to enable use of vendor primitives in e.g.
        AnalogBase

        Parameters
        ----------
        flavor: str, Default 'lvt'
        type: str, Default 'nch'

        Example: self.get_trasistor_primitives(flavor='lvt', type='nch')
        
        """
        flavor=kwargs.get('flavor','lvt')
        type=kwargs.get('type','nch')
        
        # Dictionary of cell names
        primdict={ 
                'nch' : 
                    { 
                        'lvt' : 'namefortheprimitive'
                    },
                'pch' : 
                    { 'lvt' : 'namefortheprimitive'
                    }
            }
        return primdict[type][flavor]

    @property
    def min_core_w(self):
        """ Minimum width for core transistors.
        
        """ 
        if not hasattr(self,'_min_core_w'):
            self._min_core_w= float
            return self._min_core_w
        else:
            return self._min_core_w

    def get_parallel_space_unit(self, layer_type, width, length, same_color=False):
        if not same_color or sp_sc_min not in self.config:
            config_name='sp_min'
        else: # No idea what this is
            config_name='sp_sc_min'
        sp_table=self.config[config_name]
        sp_min=sp_table[layer_type] 
        w_list=sp_min['w_list']
        l_list=sp_min['l_list']
        sp_list=sp_min['sp_list']
        if not (len(w_list) == len(l_list) == len(sp_list)):
            raise Exception("Metal spacing config. for layer type: %s contains vectors of different length!" % layer_type)
        l_idx = None
        for i in range(0, len(l_list)):
            if length <= l_list[i] and l_idx == None:
                l_idx=i
            if width <= w_list[i]:
                if l_idx == None:
                    return sp_list[i]
                # It matters not what the wire width is if the parallel run length is below a certain threshold:
                else:
                    return sp_list[l_idx]

    def get_parallel_space(self, layer_type, width, length=0, same_color=False, unit_mode=True):
        res = self.config['resolution']
        if not unit_mode:
            width=int(round(width/res))
            length=int(round(length/res))
        sp = self.get_parallel_space_unit(layer_type, width, length, same_color)
        if unit_mode:
            return sp
        else:
            return sp*res

    #OK
    def get_metal_em_specs(self, layer_name, w, l=-1, vertical=False, **kwargs):
        metal_type = self.get_layer_type(layer_name)
        idc = self._get_metal_idc(metal_type, w, l, vertical, **kwargs)
        irms = self._get_metal_irms(layer_name, w, **kwargs)
        ipeak = float('inf')
        return idc, irms, ipeak

    #OK
    def _get_metal_idc_factor(self, mtype, w, l):
        idc = inorm*self.get_metal_idc_factor(metal_type, w, l)*(w*w_shrink-woff)
        idc_temp = kwargs.get('dc_tamp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, metal_type)*idc*1e-3

    #OK
    def _get_metal_idc(self, metal_type, w, l, vertical, **kwargs):

        inorm, woff = 1.0, 0.0
        idc = inorm * self._get_metal_idc_factor(metal_type, w, l) * (w - woff)
        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, metal_type) * idc * 1e-3

    #OK
    def _get_metal_irms(self, layer_name, w, **kwargs):
        layer_id = self.get_layer_id(layer_name)
        b = float
        wscale = float

        irms_dt = kwargs.get('rms_dt', self.irms_dt)
        irms_ma = (k*irms_dt*(w*wscale-wo)**2*(w*wscale-wo+a)/(w*wscale-wo+b))**0.5
        return irms_ma*1e-3

    #OK
    def get_via_em_specs(self, via_name, bm_layer, tm_layer, via_type='square',
                         bm_dim=(-1, -1), tm_dim=(-1, -1), array=False, **kwargs):
        bm_type = self.get_layer_type(bm_layer)
        tm_type = self.get_layer_type(tm_layer)
        idc = self._get_via_idc(via_name, via_type, bm_type, tm_type, bm_dim,
                                tm_dim, array, **kwargs)
        # Vias do not have AC current specs
        irms = float('inf')
        ipeak = float('inf')
        return idc, irms, ipeak

    #OK
    def _get_via_idc(self, vname, via_type, bm_type, tm_type,
                     bm_dim, tm_dim, array, **kwargs):

        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, bm_type)*idc*1e-3

    #OK
    def get_res_em_specs(self, res_type, w, l=-1, **kwargs):
        if res_type == 'high_r' or res_type == 'standard' or res_type == 'high_speed':
            wscale = 1.0
            idc_temp = kwargs.get('dc_temp', self.idc_temp)
            idc_scale = self.get_idc_scale_factor(idc_temp, '', is_res=True)
            idc = 1.0e-3 * w*w_scale * idc_scale

            irms_dt = kwargs.get('rms_dt', self.irms_dt)
            irms = float
            ipeak = float
            return idc, irms, ipeak
        else:
            raise ValueError('Unsupported resistor type: %s' %(res_type))
    #OK
    def add_cell_boundary(self, template, box):
        # Type: (TemplateBase, BBox) -> None
        pass

    #OK
    def draw_device_blockage(self, template):
        pass
    
    def get_min_implant_space(self, width, unit_mode=True):
        """
        Return the minimum space between separate implants.
        Width is width of implant region.

        """
        sp_min_config = self.config['implant_rules']['imp_min_sp']
        w_list=sp_min_config['imp_w']
        sp_list=sp_min_config['imp_sp']
        if not unit_mode:
            width = int(width / self.resolution)
        for w, sp in zip(w_list, sp_list):
            if width <= w:
                return sp
        return None

    #OK
    def get_via_arr_enc(self, vname, vtype, mtype, mw_unit, is_bot):
        # type: (...) -> Tuple[Optional[List[Tuple[int, int]]], Optional[Callable[[int, int], bool]]]
        return None, None

