"""
==============================
BAG2 resistor parameter module
==============================

The resistor parameter template module of BAG2 framework.

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 12.4.2022.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
import yaml

#To use this file as starting point for layers_parameters class comment the following lines
from abc import ABCMeta, abstractmethod
class resistor_parameters_template(metaclass=ABCMeta):

# Then  uncomment the followint lines
#from BAG2_technology_template.resistor_parameters_template import resistor_parameters_template
#class resistor_parameters(resistor_parameters_template):

# Then, delete the lines with '@abstractmethod' and start filling in the right values

    @property
    @abstractmethod
    def bot_layer(self):
        """Bottom horizontal routing layer ID

        """
        return int 


    @property
    @abstractmethod
    def block_pitch(self):
        """Resistor core block pitch in resolution units

        """
        return (int , int )
    
    @property
    @abstractmethod
    def po_sp(self):
        """Space between PO and dummy PO

        """
        return int 

    @property
    @abstractmethod
    def imp_od_sp(self):
        """Space between implant layer and OD.  
        Used only if OD cannot be inside resistor implant.

        """
        return int 

    @property
    @abstractmethod
    def po_od_sp(self):
       """Space between PO/dummy PO and dummy OD

       """
       return int 
    @property
    @abstractmethod
    def po_co_enc(self):
        """PO horizontal/vertical enclosure of CONTACT

        """
        return [int , int ]

    @property
    @abstractmethod
    def po_rpo_ext_exact(self):
        """Exact extension of PO over RPO.  If negative, this parameter is ignored.

        """
        return -int 

    @property
    @abstractmethod
    def po_max_density(self):
        """Maximum PO density (recommended)

        """
        return float 

    @property
    @abstractmethod
    def dpo_dim_min(self):
        """Dummy PO minimum width/height

        """
        return [int , int ]

    @property
    @abstractmethod
    def od_dim_min(self):
       """Dummy OD minimum width/height

       """
       return [int , int ]

    @property
    @abstractmethod
    def od_dim_max(self):
        """Dummy OD maximum width/height

        """
        return [int , int ]

    @property
    @abstractmethod
    def od_sp(self):
        """Dummy OD space

        """
        return int 

    @property
    @abstractmethod
    def od_min_density(self):
        """Minimum OD density

        """
        return float 

    @property
    @abstractmethod
    def co_w(self):
        """CONTACT width

        """
        return int 

    @property
    @abstractmethod
    def co_sp(self):
        """CONTACT spacing

        """
        return int 

    @property
    @abstractmethod
    def m1_co_enc(self):
        """METAL1 horizontal/vertical enclosure of CONTACT

        """
        return [int , int ] 

    @property
    @abstractmethod
    def m1_sp_max(self):
        """METAL1 fill maximum spacing

        """
        return int 

    @property
    @abstractmethod
    def m1_sp_bnd(self):
        """METAL1 fill space to boundary

        """
        return int 

    @property
    @abstractmethod
    def rpo_co_sp(self):
        """Space of RPO to CONTACT

        """
        return int 

    @property
    @abstractmethod
    def rpo_extx(self):
        """Extension of RPO on PO

        """
        return int 

    @property
    @abstractmethod
    def edge_margin(self):
        """Margin needed on the edges

        """
        return int 

    @property
    @abstractmethod
    def imp_enc(self):
        """Enclosure of implant layers in horizontal/vertical direction

        """
        return [int , int ] 

    @property
    @abstractmethod
    def imp_layers(self):
        """Resistor implant layers list

        """
        return {
            'nch': {
                ('layer', 'purpose') : [int , int ],
                ('layer', 'purpose') : [int , int ],
               },
            'pch': {
               ('layer', 'purpose') : [int , int ],
               ('layer', 'purpose') : [int , int ],
               ('layer', 'purpose') : [int , int ],
              },
            'ptap': {
               ('layer', 'purpose') : [int , int ],
               ('layer', 'purpose') : [int , int ],
              },
            'ntap': {
               ('layer', 'purpose') : [int , int ],
               ('layer', 'purpose') : [int , int ],
               ('layer', 'purpose') : [int , int ],
              }
        }

    @property
    @abstractmethod
    def res_layers(self):
        return {
            'standard' : {
                ('layer', 'purpose') : [int , int ],
                },
            'high_speed' : {
                ('layer', 'purpose') : [int , int ],
                ('layer', 'purpose') : [int , int ],
                }
            }

    @property
    @abstractmethod
    def thres_layers(self):
        return {
            'ptap' : {
                'standard' : {},
                'svt' : {},
                'lvt' : {
                    ('layer', 'purpose') : [0, 0],
                    },
                'ulvt': { 
                    ('layer', 'purpose') : [0, 0],
                    },
                'fast': {
                  ('layer', 'purpose') : [0, 0],
                  },
                'hvt': {
                  ('layer', 'purpose') : [0, 0],
                  },
                'uhvt': {
                ('layer', 'purpose') : [0, 0],
                }
            },
            'ntap' : {
                'standard': {},
                'svt': {},
                'lvt': { 
                    ('layer', 'purpose') : [0, 0],
                  },
                'ulvt': { 
                    ('layer', 'purpose') : [0, 0],
                  },
                'fast': { 
                    ('layer', 'purpose') : [0, 0],
                  },
                'hvt': { 
                    ('layer', 'purpose') : [0, 0],
                  },
                'uhvt': { 
                    ('layer', 'purpose') : [0, 0],
                  },
            }
        }

    @property
    @abstractmethod
    def info(self):
        """Resistor type information dictionary
  
        """
        return {
            'standard' : {
                'rsq' : float, 
                'min_nsq' : int ,
                'w_bounds' : (float, float),
                'l_bounds' : (float, float),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo' : Bool,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy' : Bool,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res' : Bool,
            },
            'high_speed' : {
                'rsq' : float,
                'min_nsq' : float,
                'w_bounds' : (float, float),
                'l_bounds' : (float, float),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo' : Bool,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy' : Bool,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res' : Bool,
            }
        }

    @property
    @abstractmethod
    def property_dict(self):
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        devel = False

        if not hasattr(self, '_property_dict'):
            if devel:
                yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
                with open(yaml_file, 'r') as content:
                   #self._process_config = {}
                   dictionary = yaml.load(content, Loader=yaml.FullLoader )

                self._property_dict = dictionary['resistor']
            else:
                self._property_dict = {}

        for key, val  in vars(type(self)).items():
            if isinstance(val,property) and key != 'property_dict':
                self._property_dict[key] = getattr(self,key)
        return self._property_dict

