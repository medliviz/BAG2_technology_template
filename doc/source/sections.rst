BAG2 Technology templates
=========================
.. automodule:: BAG2_technology_template
   :members:
   :private-members:
   :special-members: __getattr__
   :member-order: bysource

.. automodule:: BAG2_technology_template.layer_parameters_template
   :members:
   :undoc-members:

.. automodule:: BAG2_technology_template.via_parameters_template
   :members:
   :undoc-members:

.. automodule:: BAG2_technology_template.mos_parameters_template
   :members:
   :undoc-members:

.. automodule:: BAG2_technology_template.resistor_parameters_template
   :members:
   :undoc-members:

