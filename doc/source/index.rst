.. thesdk documentation master file.
   You can adapt this file completely to your liking, 
   but it should at least contain the root `toctree` directive.

===================================
BAG2 technology definition template
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   sections

   indices_and_tables

